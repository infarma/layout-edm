package arquivoDeNotaFiscal

import (
	"fmt"
	"math/big"
	"time"
)

func GetHeaderNF( NumeroSequencialArquivo string, CGCdoFornecedor string, DataDeGeraçãoDoArquivo time.Time ) string{


	cabecalho := fmt.Sprintf("%06s", NumeroSequencialArquivo)
	//tipo de registro
	cabecalho += "01"
	//----------------
	cabecalho += fmt.Sprintf("%-14s", CGCdoFornecedor)
	cabecalho += DataDeGeraçãoDoArquivo.Format("02012006")

	return cabecalho
}

func GetNotaFiscalNF( NumeroSequencialArquivo string, NumeroDaNotaFiscal int32, DataDeEmissaoDaNotaFiscal time.Time, CGCDestinatario string, Filler string, ValorTotalDaNota int64, ValorBaseDeICMS int64, ValorDoICMS int64, ValorBaseST int64, ValorDoST int64, ValorDoIPI int64, ValorTotalDosItens int64, ValorDeVendaTotal int64, ValorTotalDescontos int64, ValorTotalFreteBoletoOutros int64, SerieDaNota int32  ) string{


	NF := fmt.Sprintf("%06s", NumeroSequencialArquivo)
	//tipo de registro
	NF += "02"
	//----------------
	NF += fmt.Sprintf("%06d", NumeroDaNotaFiscal)
	NF += DataDeEmissaoDaNotaFiscal.Format("02012006")
	NF += fmt.Sprintf("%-14s", CGCDestinatario)
	NF += fmt.Sprintf("%-6s", Filler)
	NF += fmt.Sprintf("%017d", ValorTotalDaNota)
	NF += fmt.Sprintf("%017d", ValorBaseDeICMS)
	NF += fmt.Sprintf("%017d", ValorDoICMS)
	NF += fmt.Sprintf("%017d", ValorBaseST)
	NF += fmt.Sprintf("%017d", ValorDoST)
	NF += fmt.Sprintf("%017d", ValorDoIPI)
	NF += fmt.Sprintf("%017d", ValorTotalDosItens)
	NF += fmt.Sprintf("%017d", ValorDeVendaTotal)
	NF += fmt.Sprintf("%017d", ValorTotalDescontos)
	NF += fmt.Sprintf("%017d", ValorTotalFreteBoletoOutros)
	NF += fmt.Sprintf("%03d", SerieDaNota)

	return NF
}

func GetItemNotaFiscalNF( NumeroSequencialArquivo string, NumeroDaNotaFiscal int32, DataDeEmissaoDaNotaFiscal time.Time, CodigoDoProduto int64, CodigoEAN int64, TipoDeTributo string,  AliquotaDeICMS string, Quantidade int64, ValorUnitarioBruto int64, ValorTotalIPI int64, ValorTotalDeDescontos int64, ValorUnitarioLiquido int64, ValorTotalDoItem int64, ValorDeVendaUnitario int64, ValorDoICMSSubst int64, CFOP int32, Fator float64, ValorBaseDeCalculoICMS int64, ValorBaseDeCalculoICMSST int64, ValorDoICMS int64,  ValorTotalFreteBoletoOutros int64, DataDeVencimento time.Time, NumeroDoLote int64 ) string{


	NF := fmt.Sprintf("%06s", NumeroSequencialArquivo)
	//tipo de registro
	NF += "04"
	//----------------
	NF += fmt.Sprintf("%06d", NumeroDaNotaFiscal)
	NF += DataDeEmissaoDaNotaFiscal.Format("02012006")
	NF += fmt.Sprintf("%014d", CodigoDoProduto)
	NF += fmt.Sprintf("%-14d", CodigoEAN)
	NF += fmt.Sprintf("%-1s",  TipoDeTributo)
	NF += fmt.Sprintf("%05s", AliquotaDeICMS)
	NF += fmt.Sprintf("%010d", Quantidade)
	NF += fmt.Sprintf("%017d", ValorUnitarioBruto)
	NF += fmt.Sprintf("%017d", ValorTotalIPI)
	NF += fmt.Sprintf("%017d", ValorTotalDeDescontos)
	NF += fmt.Sprintf("%017d", ValorUnitarioLiquido)
	NF += fmt.Sprintf("%017d", ValorTotalDoItem)
	NF += fmt.Sprintf("%017d", ValorDeVendaUnitario)
	NF += fmt.Sprintf("%017d", ValorDoICMSSubst)
	NF += fmt.Sprintf("%04d", CFOP)
	//4 casas decimais
	NF += fmt.Sprintf("%012s", big.NewFloat(Fator).SetMode(big.AwayFromZero).Text('f', 4))
	NF += fmt.Sprintf("%017d", ValorBaseDeCalculoICMS)
	NF += fmt.Sprintf("%017d", ValorBaseDeCalculoICMSST)
	NF += fmt.Sprintf("%017d", ValorDoICMS)
	NF += fmt.Sprintf("%017d", ValorTotalFreteBoletoOutros)
	NF += DataDeVencimento.Format("02012006")
	NF += fmt.Sprintf("%-10d", NumeroDoLote)


	return NF
}

func GetAcrescimosDescontosNF( NumeroSequencialArquivo string, NumeroDaNotaFiscal int32, DataDeEmissaoDaNotaFiscal time.Time, CodigoDoProduto int64, CodigoEAN int64, TipoDeIncidencia string, DescricaoDoValor string, Valor int64) string{


	Acrescimos := fmt.Sprintf("%06s", NumeroSequencialArquivo)
	//tipo de registro
	Acrescimos += "05"
	//----------------
	Acrescimos += fmt.Sprintf("%-6d", NumeroDaNotaFiscal)
	Acrescimos += DataDeEmissaoDaNotaFiscal.Format("02012006")
	Acrescimos += fmt.Sprintf("%014d", CodigoDoProduto)
	Acrescimos += fmt.Sprintf("%014d", CodigoEAN)
	Acrescimos += fmt.Sprintf("%-1s", TipoDeIncidencia)
	Acrescimos += fmt.Sprintf("%-15s", DescricaoDoValor)
	Acrescimos += fmt.Sprintf("%-17d", Valor)


	return Acrescimos
}


func GetDataDeVencimentoNF( NumeroSequencialArquivo string, NumeroDaNotaFiscal int32, DataDeEmissaoDaNotaFiscal time.Time, NrDaParcela int32, DataDeVencimento time.Time, Valor int64) string{


	Data := fmt.Sprintf("%06s", NumeroSequencialArquivo)
	//tipo de registro
	Data += "06"
	//----------------
	Data += fmt.Sprintf("%-6d", NumeroDaNotaFiscal)
	Data += DataDeEmissaoDaNotaFiscal.Format("02012006")
	Data += fmt.Sprintf("%02d", NrDaParcela)
	Data += DataDeVencimento.Format("02012006")
	Data += fmt.Sprintf("%-17d", Valor)


	return Data
}

func GetTrailerNF( NumeroSequencialArquivo string, ValorTotalDasNF int64, NumeroDeRegistrosTipo2 int32, NumeroDeRegistrosTipo3 int32, NumeroDeRegistrosTipo4 int32, NumeroDeRegistrosTipo5 int32) string{


	Trailer := fmt.Sprintf("%06s", NumeroSequencialArquivo)
	//tipo de registro
	Trailer += "99"
	//----------------
	Trailer += fmt.Sprintf("%017d", ValorTotalDasNF)
	Trailer += fmt.Sprintf("%-5d", NumeroDeRegistrosTipo2)
	Trailer += fmt.Sprintf("%-5d", NumeroDeRegistrosTipo3)
	Trailer += fmt.Sprintf("%-5d", NumeroDeRegistrosTipo4)
	Trailer += fmt.Sprintf("%-5d", NumeroDeRegistrosTipo5)

	return Trailer
}


